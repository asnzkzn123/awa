using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bs : MonoBehaviour
{
    public static Bs BsInstance;

    private void Awake()
    {
        if (BsInstance != null && BsInstance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        BsInstance = this;

        DontDestroyOnLoad(this);
    }
}
